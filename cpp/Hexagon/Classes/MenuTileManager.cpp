#include "MenuTileManager.h"

USING_NS_CC;

typedef std::map<int, std::map<int, MenuTile*>>::iterator it_typeX;
typedef std::map<int, MenuTile*>::iterator it_typeY;

bool MenuTileManager::init()
{
	if (!Layer::init())
		return false;

	auto size = Director::getInstance()->getWinSize();

	this->TILE_WINDOW_X = size.width - 620 / Director::getInstance()->getContentScaleFactor();
	this->TILE_WINDOW_Y = 36;
	this->TILE_WIDTH = 169 / Director::getInstance()->getContentScaleFactor();
	this->TILE_HEIGHT = 146 / Director::getInstance()->getContentScaleFactor();
	this->TILE_SPACING = 30 / Director::getInstance()->getContentScaleFactor();


	this->generateTileAtLocation(cocos2d::Point(0,2), 0.1f);
	this->generateTileAtLocation(cocos2d::Point(1,0), 0.3f);
	this->generateTileAtLocation(cocos2d::Point(1,1), 0.2f);
	this->generateTileAtLocation(cocos2d::Point(1,2), 0.2f);
	this->generateTileAtLocation(cocos2d::Point(1,3), 0.3f);
	this->generateTileAtLocation(cocos2d::Point(2,1), 0.4f);
	this->generateTileAtLocation(cocos2d::Point(2,2), 0.3f);
	this->generateTileAtLocation(cocos2d::Point(2,3), 0.4f);
	this->generateTileAtLocation(cocos2d::Point(3,0), 0.5f);
	this->generateTileAtLocation(cocos2d::Point(3,1), 0.4f);
	this->generateTileAtLocation(cocos2d::Point(3,2), 0.4f);
	this->generateTileAtLocation(cocos2d::Point(3,3), 0.5f);
	this->generateTileAtLocation(cocos2d::Point(4,0), 0.6f);
	this->generateTileAtLocation(cocos2d::Point(4,1), 0.5f);
	this->generateTileAtLocation(cocos2d::Point(4,2), 0.5f);
	this->generateTileAtLocation(cocos2d::Point(4,3), 0.5f);


	this->positionTiles();

	this->schedule(schedule_selector(MenuTileManager::flicker), 1.5);

	return true;
}

void MenuTileManager::flicker(float dt)
{
	this->schedule(schedule_selector(MenuTileManager::flicker), rand() % 6 + 5);
	for (it_typeX iteratorX = this->tileMap.begin(); iteratorX != this->tileMap.end(); iteratorX++)
	{
		std::map<int, MenuTile*> mapY = iteratorX->second;
		for (it_typeY iteratorY = mapY.begin(); iteratorY != mapY.end(); iteratorY++)
		{
			MenuTile* tile = iteratorY->second;
			tile->flicker();
		}
	}
}

void MenuTileManager::generateTileAtLocation(cocos2d::Point location, float delay)
{
	auto newTile = MenuTile::createWithColor(BLANK);
	newTile->setGridPosition(location);
	newTile->setFlickerDelay(delay);
	this->addChild(newTile);
	this->tileMap[(int)location.x][(int)location.y] = newTile;

	CCLOG("New tile at x:%d y:%d", (int)location.x, (int)location.y);
}

void MenuTileManager::positionTiles()
{
	int columnShift = 0;
	
	for (it_typeX iteratorX = this->tileMap.begin(); iteratorX != this->tileMap.end(); iteratorX++)
	{
		std::map<int, MenuTile*> mapY = iteratorX->second;
		for (it_typeY iteratorY = mapY.begin(); iteratorY != mapY.end(); iteratorY++)
		{
			CCLOG("Position x:%d y:%d", iteratorX->first, iteratorY->first);
			MenuTile* tile = iteratorY->second;
			if (iteratorX->first % 2 == 0)
				columnShift = 0;
			else
				columnShift = (this->TILE_HEIGHT + this->TILE_SPACING) / 2;

			tile->setPosition(this->TILE_WINDOW_X + iteratorX->first * (this->TILE_WIDTH - this->TILE_WIDTH / 4 + this->TILE_SPACING), this->TILE_WINDOW_Y + iteratorY->first * (this->TILE_HEIGHT + this->TILE_SPACING) + columnShift);
		}
	}
}