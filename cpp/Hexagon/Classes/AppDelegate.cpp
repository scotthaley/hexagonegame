#include "AppDelegate.h"
#include "IntroLayer.h"

#include <ctime>

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("Hexagon");
		glview->setFrameSize(1136,640);				/* iPhone 5 */
		//glview->setFrameSize(1280,752);				/* Asus Transformer */
		//glview->setFrameSize(1920,1080);				/* Galaxy s4 */
        director->setOpenGLView(glview);
    }

	auto fileUtils = FileUtils::getInstance();
    std::vector<std::string> searchPaths;
	searchPaths.push_back("Game");
	searchPaths.push_back("Game/images");
	searchPaths.push_back("Game/images/home");
	searchPaths.push_back("Game/images/game");

	fileUtils->setSearchPaths(searchPaths);

	auto screenSize = glview->getFrameSize();
	director->setContentScaleFactor(640 / screenSize.height);

	CCLOG("Screen size: %f, %f", screenSize.width, screenSize.height);

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
	auto scene = IntroLayer::createScene();

	srand( time(NULL));

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
