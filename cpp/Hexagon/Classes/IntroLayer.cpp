#include "IntroLayer.h"
#include "MenuLayer.h"

USING_NS_CC;

cocos2d::Scene* IntroLayer::createScene()
{
	auto scene = Scene::create();
	auto layer = IntroLayer::create();
	scene->addChild(layer);
	return scene;
}

bool IntroLayer::init()
{
	if (!LayerColor::initWithColor(Color4B(54,75,97,255)))
		return false;

	auto size = Director::getInstance()->getWinSize();

	auto background = Sprite::create("home-hexagone-logo.png");
	background->setPosition(size.width / 2, size.height / 2);

	this->addChild(background);

	this->schedule(schedule_selector(IntroLayer::makeTransition), 1);

	return true;
}

void IntroLayer::makeTransition(float dt)
{
	Director::getInstance()->replaceScene(CCTransitionFade::create(1.0, MenuLayer::createScene(), Color3B(54,75,97)));
}