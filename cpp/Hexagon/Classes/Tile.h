#include "cocos2d.h"

enum tileColor
{
	BLANK = 0,
	BLUE,
	GREEN,
	RED,
	YELLOW,
	ORANGE,
	PURPLE
};

class Tile : public cocos2d::Node
{
protected:
	cocos2d::Sprite* sprite;
	int GridRotation;

public:
	float scoreMultiplier;
	bool isDragging;
	tileColor Color;
	bool Destroyed;
	bool NewTile;

	cocos2d::Size getSize();
	cocos2d::Vector<Tile*> setChildGridPositions(bool evenColumn, std::map<int, std::map<int, Tile*>>);
	void destroyTile(float delay);
	void setOpacity(float opacity);
	void setGridRotation(int angle);

	
	static Tile* randomTile();
	static Tile* randomTileWithDelay(float delay);
	static Tile* randomTileWithDelayInitial(float delay);

	virtual bool initWithColor(tileColor color);
private:
	std::string getImageForColor(tileColor color);
};