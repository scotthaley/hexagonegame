#include "cocos2d.h"

class IntroLayer : public cocos2d::LayerColor
{
private:
	void makeTransition(float dt);
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(IntroLayer);
};