#include "cocos2d.h"
#include "MenuTile.h"

class MenuTileManager : public cocos2d::Layer
{
protected:
	std::map<int, std::map<int, MenuTile*>> tileMap;

	int TILE_WINDOW_X;
	int TILE_WINDOW_Y;
	int TILE_WIDTH;
	int TILE_HEIGHT;
	int TILE_SPACING;

public:
	virtual bool init();

	CREATE_FUNC(MenuTileManager);
	
private:
	void generateTileAtLocation(cocos2d::Point location, float delay);
	void positionTiles();
	void flicker(float dt);
	MenuTile* tileAtPoint(cocos2d::Point location);
};