#include "cocos2d.h"

enum tileColor
{
	BLANK = 0,
	BLUE,
	GREEN,
	RED,
	YELLOW,
	ORANGE,
	PURPLE
};

class MenuTile : public cocos2d::Node
{
protected:
	cocos2d::Sprite* sprite;
	cocos2d::Sprite* flickerSprite;
	cocos2d::Point gridPosition;
	float flickerDelay;

public:
	void flicker();
	void flickerAction();

	void setGridPosition(cocos2d::Point);
	void setFlickerDelay(float);
	
	static MenuTile* createWithColor(tileColor color);
	virtual bool init(tileColor color);

	//CREATE_FUNC(MenuTile);

private:
	std::string getImageForColor(tileColor color);
};