#include "MenuTile.h"

USING_NS_CC;

MenuTile* MenuTile::createWithColor(tileColor color)
{
	MenuTile* newMenuTile = new MenuTile();
	if (newMenuTile && newMenuTile->init(color))
	{
		newMenuTile->autorelease();
		return newMenuTile;
	}
	CC_SAFE_DELETE(newMenuTile);
	return NULL;
}

bool MenuTile::init(tileColor color)
{
	if (!Node::init())
		return false;

	this->sprite = Sprite::create(this->getImageForColor(color));
	this->sprite->setOpacity(0);
	this->addChild(this->sprite);

	this->flickerSprite = Sprite::create(this->getImageForColor(BLANK));
	this->flickerSprite->setOpacity(0);
	this->addChild(this->flickerSprite);

	this->setContentSize(this->sprite->getContentSize());

	return true;
}

void MenuTile::flicker()
{
	//this->runAction(CCSequence::create(CCDelayTime::create(this->flickerDelay * 1), CCCallFunc::create(this, callfunc_selector(MenuTile::flickerAction)), NULL));
	//std::function<void> fA = std::bind(&MenuTile::setFlickerDelay, this);
	this->runAction(CCSequence::create(CCDelayTime::create(this->flickerDelay * 1), CallFunc::create( CC_CALLBACK_0(MenuTile::flickerAction, this)), NULL));
}

void MenuTile::flickerAction()
{
	std::string newImage = FileUtils::getInstance()->fullPathForFilename(this->getImageForColor(static_cast<tileColor>(rand() % 6 + 1)));
	this->flickerSprite->setTexture(Director::getInstance()->getTextureCache()->addImage(newImage));
	this->flickerSprite->runAction(CCSequence::create(CCFadeIn::create(0.4f), CCDelayTime::create(0.01f), CCFadeOut::create(0.4f), NULL));
	this->sprite->setOpacity(255);
}

void MenuTile::setGridPosition(cocos2d::Point location)
{
	this->gridPosition = location;
}

void MenuTile::setFlickerDelay(float delay)
{
	this->flickerDelay = delay;
}

std::string MenuTile::getImageForColor(tileColor color)
{
	switch(color)
	{
	case BLUE:
		return "game-blue-shape.png";
		break;
	case GREEN:
		return "game-green-shape.png";
		break;
	case RED:
		return "game-red-shape.png";
		break;
	case YELLOW:
		return "game-yellow-shape.png";
		break;
	case ORANGE:
		return "game-orange-shape.png";
		break;
	case PURPLE:
		return "game-purple-shape.png";
		break;
	case BLANK:
		return "home-hexagon-bg.png";
		break;
	default:
		return "home-hexagon-bg.png";
		break;
	};
}