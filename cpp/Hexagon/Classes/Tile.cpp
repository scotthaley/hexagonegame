#include "Tile.h"

Tile* Tile::randomTile()
{
	Tile* newTile = new Tile();
	if (newTile && newTile->initWithColor(static_cast<tileColor>(rand() % 6 + 1)))
	{
		newTile->autorelease();
		return newTile;
	}
	CC_SAFE_DELETE(newTile);
	return NULL;
}

Tile* Tile::randomTileWithDelay(float delay)
{
	Tile* newTile = Tile::randomTile();
	newTile->NewTile = true;
	newTile->setOpacity(0);
	newTile->runAction(cocos2d::Sequence::create(cocos2d::DelayTime::create(delay), cocos2d::FadeIn::create(0.2), NULL));
	return newTile;
}

Tile* Tile::randomTileWithDelayInitial(float delay)
{
	Tile* newTile = Tile::randomTile();
	newTile->setOpacity(0);
	newTile->runAction(cocos2d::Sequence::create(cocos2d::DelayTime::create(delay), cocos2d::FadeIn::create(0.2), NULL));
	return newTile;
}

bool Tile::initWithColor(tileColor color)
{
	if (!Node::init())
		return false;

	return true;
}

void Tile::setOpacity(float opacity)
{
	this->sprite->setOpacity(opacity);
}

std::string Tile::getImageForColor(tileColor color)
{
	switch (color)
	{
	case BLUE:
		return "game-blue-shape.png";
		break;
	case GREEN:
		return "game-green-shape.png";
		break;
	case RED:
		return "game-red-shape.png";
		break;
	case YELLOW:
		return "game-yellow-shape.png";
		break;
	case ORANGE:
		return "game-orange-shape.png";
		break;
	case PURPLE:
		return "game-purple-shape.png";
		break;
	case BLANK:
		return "home-hexagon-bg.png";
		break;
	default:
		return "home-hexagon-bg.png";
		break;
	};
}