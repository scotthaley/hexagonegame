#include "cocos2d.h"
#include "MenuTileManager.h"

class MenuLayer : public cocos2d::LayerColor
{
protected:
	MenuTileManager* TileManager;

	cocos2d::Sprite* PlayButton;
	cocos2d::Sprite* GameCenter;
	cocos2d::Sprite* Challenges;

	cocos2d::Node* GameTypeNode;
	cocos2d::Sprite* GameTypeBG;
	cocos2d::Sprite* TimedType;
	cocos2d::Sprite* TurnsType;
	cocos2d::Sprite* EndlessType;

	cocos2d::LayerColor* Overlay;

	bool typeSelectOpen;
	
public:
	static cocos2d::Scene* createScene();
	virtual bool init();

	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);

	CREATE_FUNC(MenuLayer);

private:
	void startTimed();
	
};