#include "MenuLayer.h"

USING_NS_CC;

Scene* MenuLayer::createScene()
{
	auto scene = Scene::create();
	auto layer = MenuLayer::create();
	scene->addChild(layer);
	return scene;
}

bool MenuLayer::init()
{
	if (!LayerColor::initWithColor(Color4B(54,75,97,255)))
		return false;

	auto scale = Director::getInstance()->getContentScaleFactor();

	//auto logo = Sprite::create("Game/images/home/home-hexagone-logo.png");
	auto logo = Sprite::create("home-hexagone-logo.png");
	logo->setPosition(272 / scale,430 / scale);
	this->addChild(logo);

	this->PlayButton = Sprite::create("home-play-btn.png");
	this->PlayButton->setPosition(220 / scale,156 / scale);
	this->PlayButton->setOpacity(0);
	this->addChild(this->PlayButton);
	this->PlayButton->runAction(CCSequence::create(CCDelayTime::create(1), CCFadeIn::create(0.3f), NULL));

	this->GameCenter = Sprite::create("home-gamecenter-btn.png");
	this->GameCenter->setPosition(220 / scale + 140 / scale,156 / scale + 54 / scale);
	this->GameCenter->setOpacity(0);
	this->addChild(this->GameCenter);
	this->GameCenter->runAction(CCSequence::create(CCDelayTime::create(1.5f), CCFadeIn::create(0.3f), NULL));

	this->Challenges = Sprite::create("home-challenges-btn.png");
	this->Challenges->setPosition(220 / scale + 140 / scale,156 / scale - 54 / scale);
	this->Challenges->setOpacity(0);
	this->addChild(this->Challenges);
	this->Challenges->runAction(CCSequence::create(CCDelayTime::create(1.5f), CCFadeIn::create(0.3f), NULL));

	this->TileManager = MenuTileManager::create();
	this->addChild(this->TileManager);

	this->Overlay = LayerColor::create(Color4B(0,0,0,0));
	this->addChild(this->Overlay);

	this->GameTypeBG = Sprite::create("home-gametype-bg.png");
	this->GameTypeBG->setAnchorPoint(Point(0,0));
	this->addChild(this->GameTypeBG);

	this->GameTypeNode = Node::create();
	this->GameTypeNode->setAnchorPoint(Point(0,0));
	this->addChild(this->GameTypeNode);

	auto size = Director::getInstance()->getWinSize();

	this->TimedType = Sprite::create("home-timed-btn.png");
	this->TimedType->setAnchorPoint(Point(0,0.5f));
	this->TimedType->setPosition(0,size.height / 2 + 120);
	this->GameTypeNode->addChild(this->TimedType);

	this->TurnsType = Sprite::create("home-turns-btn.png");
	this->TurnsType->setAnchorPoint(Point(0,0.5f));
	this->TurnsType->setPosition(0, size.height / 2);
	this->GameTypeNode->addChild(this->TurnsType);

	this->EndlessType = Sprite::create("home-endless-btn.png");
	this->EndlessType->setAnchorPoint(Point(0,0.5f));
	this->EndlessType->setPosition(0, size.height / 2 - 120);
	this->GameTypeNode->addChild(this->EndlessType);

	this->GameTypeNode->setPosition(-this->GameTypeBG->getBoundingBox().size.width, 0);
	this->GameTypeBG->setPosition(this->GameTypeNode->getPosition());

	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);  
    listener->onTouchBegan = CC_CALLBACK_2(MenuLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	this->typeSelectOpen = false;

	return true;
}


bool MenuLayer::onTouchBegan(Touch* touch, Event* event)
{
	auto location = touch->getLocation();

	if (this->typeSelectOpen)
	{
		if (location.x > 568)
		{
			this->GameTypeNode->runAction(CCEaseOut::create(CCMoveTo::create(0.4f, Point(-this->EndlessType->getBoundingBox().size.width * 1.5,0)), 3));
			this->GameTypeBG->runAction(CCEaseOut::create(CCMoveTo::create(0.4f, Point(-this->GameTypeBG->getBoundingBox().size.width, 0)), 3));
			this->Overlay->runAction(CCFadeTo::create(0.4f, 0));

			this->typeSelectOpen = false;
		}
	} else {
		if(this->PlayButton->getBoundingBox().containsPoint(convertTouchToNodeSpace(touch)))
		{
			this->GameTypeNode->runAction(CCEaseOut::create(CCMoveTo::create(0.4f, Point(0,0)), 3));
			this->GameTypeBG->runAction(CCEaseOut::create(CCMoveTo::create(0.4f, Point(-1136 / Director::getInstance()->getContentScaleFactor(), 0)), 3));
			this->Overlay->runAction(CCFadeTo::create(0.4f, 125));

			this->typeSelectOpen = true;
		}
	}

	return true;
}

void MenuLayer::startTimed()
{

}